package com.humanbooster.benjamin.service;

import com.humanbooster.benjamin.business.Passager;

import java.util.List;
import java.util.Optional;

public interface PassagerService {

	Passager creerPassager(Passager passager);
	
	Optional<Passager> recupererPassager(Long id);
	
	List<Passager> recupererPassagers();
	
	Passager majPassager(Passager passager);
	
	boolean supprimerPassager(Long id);
}
