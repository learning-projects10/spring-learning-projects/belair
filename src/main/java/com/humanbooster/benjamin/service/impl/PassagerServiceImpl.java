package com.humanbooster.benjamin.service.impl;


import com.humanbooster.benjamin.business.Passager;
import com.humanbooster.benjamin.dao.PassagerDao;
import com.humanbooster.benjamin.service.PassagerService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PassagerServiceImpl implements PassagerService {

	private final PassagerDao passagerDao;

	public PassagerServiceImpl(PassagerDao passagerDao) {
		this.passagerDao = passagerDao;
	}

	@Override
	public Passager creerPassager(Passager passager) {
		return passagerDao.save(passager);
	}

	@Override
	public Optional<Passager> recupererPassager(Long id) {
		return passagerDao.findById(id);
	}

	@Override
	public List<Passager> recupererPassagers() {
		return passagerDao.findAll();
	}

	@Override
	public Passager majPassager(Passager passager) {
		return passagerDao.save(passager);
	}

	@Override
	public boolean supprimerPassager(Long id) {
		if (recupererPassager(id).isPresent()) {
			passagerDao.deleteById(id);
			return true;
		}
		return false;
	}

}
