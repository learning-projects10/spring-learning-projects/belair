package com.humanbooster.benjamin.service;

import com.humanbooster.benjamin.business.Service;

import java.util.List;
import java.util.Optional;

public interface ServiceService {

	Service creerService(Service service);
	
	Optional<Service> recupererService(Long id);
	
	List<Service> recupererServices();
	
	Service majService(Service service);
	
	boolean supprimerService(Long id);
}
