package com.humanbooster.benjamin.service;

import com.humanbooster.benjamin.business.Aerodrome;

import java.util.List;
import java.util.Optional;

public interface AerodromeService {

	Aerodrome creerAerodrome(Aerodrome aerodrome);
	
	Optional<Aerodrome> recupererAerodrome(Long id);
	
	List<Aerodrome> recupererAerodromes();
	
	Aerodrome majAerodromen(Aerodrome aerodrome);
	
	boolean supprimerAerodrome(Long id);

}
