package com.humanbooster.benjamin.service;

import com.humanbooster.benjamin.business.Reservation;

import java.util.List;
import java.util.Optional;

public interface ReservationService {

	Reservation creerReservation(Reservation reservation);
	
	Optional<Reservation> recupererReservation(Long id);
	
	List<Reservation> recupererReservations();
	
	Reservation majReservation(Reservation reservation);
	
	boolean supprimerReservation(Long id);
}
