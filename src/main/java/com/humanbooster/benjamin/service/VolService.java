package com.humanbooster.benjamin.service;

import com.humanbooster.benjamin.business.Vol;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface VolService {

	Vol creerVol(Vol vol);
	
	Optional<Vol> recupererVol(Long id);
	
	List<Vol> recupererVols();
	
	Page<Vol> recupererVols(Pageable pageable);
	
	Vol majVol(Vol vol);
	
	boolean supprimerVol(Long id);
}
