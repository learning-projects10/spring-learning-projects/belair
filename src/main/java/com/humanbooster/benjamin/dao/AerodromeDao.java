package com.humanbooster.benjamin.dao;

import com.humanbooster.benjamin.business.Aerodrome;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AerodromeDao extends JpaRepository<Aerodrome, Long> {

}
