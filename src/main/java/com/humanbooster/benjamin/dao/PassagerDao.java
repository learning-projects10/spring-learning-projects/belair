package com.humanbooster.benjamin.dao;


import com.humanbooster.benjamin.business.Passager;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PassagerDao extends JpaRepository<Passager, Long> {

}
