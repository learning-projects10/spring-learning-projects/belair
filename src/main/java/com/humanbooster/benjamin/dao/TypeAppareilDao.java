package com.humanbooster.benjamin.dao;


import com.humanbooster.benjamin.business.TypeAppareil;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TypeAppareilDao extends JpaRepository<TypeAppareil, Long> {

}
