package com.humanbooster.benjamin.dao;


import com.humanbooster.benjamin.business.Service;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceDao extends JpaRepository<Service, Long> {

}
