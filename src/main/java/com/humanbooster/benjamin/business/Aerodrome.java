package com.humanbooster.benjamin.business;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class Aerodrome {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@NotBlank(message="Le nom ne peut pas être vide.")
	private String nom;
	
	public Aerodrome() {
		// TODO Auto-generated constructor stub
	}

	public Aerodrome(String nom) {
		super();
		this.nom = nom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Aerodrome [id=" + id + ", nom=" + nom + "]";
	}
	
}
