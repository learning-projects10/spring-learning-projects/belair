package com.humanbooster.benjamin.business;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

@Entity
public class Service {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private String nom;
	
	@Min(1)
	@Max(10)
	private float prixEnEuros;
	
	@ManyToMany(mappedBy="services")
	private List<Reservation> reservations;
	
	public Service() {
		// TODO Auto-generated constructor stub
	}

	public Service(String nom) {
		super();
		this.nom = nom;
	}

	public Service(String nom, @Min(1) @Max(10) float prixEnEuros) {
		super();
		this.nom = nom;
		this.prixEnEuros = prixEnEuros;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public float getPrixEnEuros() {
		return prixEnEuros;
	}

	public String getNomEtPrixEnEuros() {
		return nom + " (" + prixEnEuros + " €)";
	}

	public void setPrixEnEuros(float prixEnEuros) {
		this.prixEnEuros = prixEnEuros;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	@Override
	public String toString() {
		return "Service [id=" + id + ", nom=" + nom + ", prixEnEuros=" + prixEnEuros + "]";
	}
	
	
}
