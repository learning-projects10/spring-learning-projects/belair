package com.humanbooster.benjamin.business;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;

@Entity

public class Avion {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Pattern(regexp = "^F-[A-Za-z]{3}[0-9]$", message = "Numéro d''immatriculation invalide")
	private String immatriculation;

	@Past
	@Temporal(TemporalType.DATE)
	private Date datePremierVol;
	
	@Lob
	private String remarques;
	
	@NotNull(message="Le type d'appareil ne peut pas être vide.")
	@ManyToOne
	private TypeAppareil typeAppareil;
	
	@OneToMany(mappedBy="avion", fetch=FetchType.EAGER)
	private List<Vol> vols;
	
	public Avion() {
		// TODO Auto-generated constructor stub
	}
	
	public Avion(String immatriculation, TypeAppareil typeAppareil) {
		super();
		this.immatriculation = immatriculation;
		this.typeAppareil = typeAppareil;
	}

	public Avion(
			@Pattern(regexp = "^F-[A-Za-z]{3}[0-9]$", message = "Numéro d''immatriculation invalide") String immatriculation,
			@Past Date datePremierVol, @NotNull TypeAppareil typeAppareil) {
		super();
		this.immatriculation = immatriculation;
		this.datePremierVol = datePremierVol;
		this.typeAppareil = typeAppareil;
	}

	public Avion(Date datePremierVol, TypeAppareil typeAppareil) {
		super();
		this.datePremierVol = datePremierVol;
		this.typeAppareil = typeAppareil;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImmatriculation() {
		return immatriculation;
	}

	public void setImmatriculation(String immatriculation) {
		this.immatriculation = immatriculation;
	}

	public Date getDatePremierVol() {
		return datePremierVol;
	}

	public void setDatePremierVol(Date datePremierVol) {
		this.datePremierVol = datePremierVol;
	}

	public String getRemarques() {
		return remarques;
	}

	public void setRemarques(String remarques) {
		this.remarques = remarques;
	}

	public TypeAppareil getTypeAppareil() {
		return typeAppareil;
	}

	public void setTypeAppareil(TypeAppareil typeAppareil) {
		this.typeAppareil = typeAppareil;
	}

	public List<Vol> getVols() {
		return vols;
	}

	public void setVols(List<Vol> vols) {
		this.vols = vols;
	}

	@Override
	public String toString() {
		return "Avion [id=" + id + ", immatriculation=" + immatriculation + ", datePremierVol=" + datePremierVol
				+ ", remarques=" + remarques + "]";
	}
	
	
}
