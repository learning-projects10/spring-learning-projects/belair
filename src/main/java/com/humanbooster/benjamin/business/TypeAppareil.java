package com.humanbooster.benjamin.business;

import javax.persistence.*;
import java.util.List;

@Entity
public class TypeAppareil {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String nom;
	
	private int nbSieges;
	
	@OneToMany(mappedBy="typeAppareil", fetch=FetchType.EAGER)
	private List<Avion> avions;
	
	public TypeAppareil() {
		// TODO Auto-generated constructor stub
	}

	public TypeAppareil(String nom, int nbSieges) {
		super();
		this.nom = nom;
		this.nbSieges = nbSieges;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNbSieges() {
		return nbSieges;
	}

	public void setNbSieges(int nbSieges) {
		this.nbSieges = nbSieges;
	}

	@Override
	public String toString() {
		return "TypeAppareil [id=" + id + ", nom=" + nom + ", nbSieges=" + nbSieges + "]";
	}
	
	
}
