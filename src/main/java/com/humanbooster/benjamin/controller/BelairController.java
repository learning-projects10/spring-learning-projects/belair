package com.humanbooster.benjamin.controller;

import com.humanbooster.benjamin.business.*;
import com.humanbooster.benjamin.service.*;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Controller
@RequestMapping("/")
public class BelairController {
	
	private static final int NB_VOLS_PAR_PAGE = 4;
	
	private final TypeAppareilService typeAppareilService;
	private final AvionService avionService;
	private final AerodromeService aerodromeService;
	private final PassagerService passagerService;
	private final ServiceService serviceService;
	private final DureeService dureeService;
	private final VolService volService;
	private final ReservationService reservationService;
	
	private static final Calendar calendar = Calendar.getInstance();
	private static final Random random = new Random();

	public BelairController(TypeAppareilService typeAppareilService, AvionService avionService, AerodromeService aerodromeService, PassagerService passagerService, ServiceService serviceService, DureeService dureeService, VolService volService, ReservationService reservationService) {
		this.typeAppareilService = typeAppareilService;
		this.avionService = avionService;
		this.aerodromeService = aerodromeService;
		this.passagerService = passagerService;
		this.serviceService = serviceService;
		this.dureeService = dureeService;
		this.volService = volService;
		this.reservationService = reservationService;
	}

	@RequestMapping(value = { "/index", "/"})
	public ModelAndView accueil(@PageableDefault(size = NB_VOLS_PAR_PAGE) Pageable pageable) {
		ModelAndView mav = new ModelAndView("index");
		mav.addObject("vols", volService.recupererVols());
		mav.addObject("pageDeVols", volService.recupererVols(pageable));
		return mav;
	}
	
	@GetMapping("/flotte")
	public ModelAndView flotteGet() {
		ModelAndView mav = new ModelAndView("flotte");
		mav.addObject("avions", avionService.recupererAvions());
		return mav;
	}
	
	@GetMapping("/ajoutAerodrome")
	public ModelAndView ajoutAerodromeGet() {
		ModelAndView mav = new ModelAndView("ajoutAerodrome");
		mav.addObject("aerodrome", new Aerodrome());
		return mav;
	}
	
	@PostMapping("/ajoutAerodrome")
	public ModelAndView ajoutAerodromePost(@Valid @ModelAttribute Aerodrome aerodrome, BindingResult result) {
		if (result.hasErrors()) {
			ModelAndView mav = ajoutAerodromeGet();
			mav.addObject("aerodrome", aerodrome);
			return mav;
		}
		else  {
			aerodromeService.creerAerodrome(aerodrome);
			Pageable pageable = PageRequest.of(0, NB_VOLS_PAR_PAGE);
			ModelAndView mav = accueil(pageable);
			mav.addObject("aerodromeAdd", "L'aérodrome " + aerodrome.getNom() + " a bien été ajouté !");
			return mav;
		}
	}
	
	@GetMapping("/ajoutAvion")
	public ModelAndView ajoutAvionGet() {
		ModelAndView mav = new ModelAndView("ajoutAvion");
		mav.addObject("typeAppareils", typeAppareilService.recupererTypeAppareils());
		mav.addObject("avion", new Avion());
		return mav;
	}
	
	@PostMapping("/ajoutAvion")
	public ModelAndView ajoutAvionPost(@Valid @ModelAttribute Avion avion, BindingResult result) {
		if (result.hasErrors()) {
			ModelAndView mav = ajoutAvionGet();
			mav.addObject("avion", avion);
			return mav;
		}
		else  {
			avionService.creerAvion(avion);
			Pageable pageable = PageRequest.of(0, NB_VOLS_PAR_PAGE);
			ModelAndView mav = accueil(pageable);
			mav.addObject("avionAdd", "L'avion " + avion.getImmatriculation() + " a bien été ajouté !");
			return mav;
		}
	}
	
	@GetMapping("/ajoutVol")
	public ModelAndView ajoutVolGet() {
		ModelAndView mav = new ModelAndView("ajoutVol");
		mav.addObject("aerodromes", aerodromeService.recupererAerodromes());
		mav.addObject("avions", avionService.recupererAvions());
		mav.addObject("vol", new Vol());
		return mav;
	}
	
	@PostMapping("/ajoutVol")
	public ModelAndView ajoutVolPost(@Valid @ModelAttribute Vol vol, BindingResult result) {
		if (result.hasErrors()) {
			ModelAndView mav = ajoutVolGet();
			mav.addObject("vol", vol);
			return mav;
		}
		else  {
			volService.creerVol(vol);
			Pageable pageable = PageRequest.of(0, NB_VOLS_PAR_PAGE);
			ModelAndView mav = accueil(pageable);
			mav.addObject("volAdd", "Le vol a bien été ajouté !");
			return mav;
		}
	}
	
	@GetMapping("/reservation")
	public ModelAndView reservationGet(@RequestParam("ID") Long idVol) {
		ModelAndView mav = new ModelAndView("reservation");
		Vol vol = volService.recupererVol(idVol).get();
		Reservation reservation = new Reservation();
		reservation.setVol(vol);
		mav.addObject("passagers", passagerService.recupererPassagers());
		mav.addObject("services", serviceService.recupererServices());
		mav.addObject("reservation", reservation);
		return mav;
	}
	
	@PostMapping("/reservation")
	public ModelAndView reservationPost(@Valid @ModelAttribute Reservation reservation, BindingResult result) {
		if (result.hasErrors()) {
			ModelAndView mav = reservationGet(reservation.getVol().getId());
			Vol vol = volService.recupererVol(reservation.getVol().getId()).get();
			reservation.setVol(vol);
			mav.addObject("reservation", reservation);
			return mav;
		}
		else  {
			reservation.setDateHeureReservation(new Date());
			reservationService.creerReservation(reservation);
			Pageable pageable = PageRequest.of(0, NB_VOLS_PAR_PAGE);
			ModelAndView mav = accueil(pageable);
			mav.addObject("reservationAdd", "La réservation a bien été enregistré !");
			return mav;
		}
	}
	
	@PostConstruct
	private void init() {
		if (typeAppareilService.recupererTypeAppareils().isEmpty()) {
			typeAppareilService.creerTypeAppareil(new TypeAppareil("Quest Kodiak Series II", 10));	
			typeAppareilService.creerTypeAppareil(new TypeAppareil("Diamond DA62 NXi", 9));	
		}
		if (avionService.recupererAvions().isEmpty()) {
			avionService.creerAvion(new Avion("F-AJD0", typeAppareilService.recupererTypeAppareil(1l).get()));
			avionService.creerAvion(new Avion("F-AJD1", typeAppareilService.recupererTypeAppareil(1l).get()));
			avionService.creerAvion(new Avion("F-AJD2", typeAppareilService.recupererTypeAppareil(2l).get()));
			avionService.creerAvion(new Avion("F-AJD3", typeAppareilService.recupererTypeAppareil(2l).get()));
		}
		if (aerodromeService.recupererAerodromes().isEmpty()) {
			aerodromeService.creerAerodrome(new Aerodrome("Frans"));
			aerodromeService.creerAerodrome(new Aerodrome("La Tour-du-Pin"));
			aerodromeService.creerAerodrome(new Aerodrome("Saint-Chamond"));
			aerodromeService.creerAerodrome(new Aerodrome("Saint-Etienne"));
			aerodromeService.creerAerodrome(new Aerodrome("Villeurbanne"));
		}
		if (passagerService.recupererPassagers().isEmpty()) {
			for (int i = 0; i < 12; i++) {
				passagerService.creerPassager(new Passager("Parsy"+(i+1),"Benjamin"+(i+1)));
			}
		}
		if (serviceService.recupererServices().isEmpty()) {
			serviceService.creerService(new Service("Massage des cervicales", 3));
			serviceService.creerService(new Service("Kiwi", 5));
			serviceService.creerService(new Service("Poulet roti fermier bio", 8));
			serviceService.creerService(new Service("Bière Happy Spring", 2));
			serviceService.creerService(new Service("Bobun et mojito", 6));
		}
		if (dureeService.recupererDurees().isEmpty()) {
			dureeService.creerDuree(new Duree(aerodromeService.recupererAerodrome(1l).get(), aerodromeService.recupererAerodrome(2l).get(), 120));
			dureeService.creerDuree(new Duree(aerodromeService.recupererAerodrome(3l).get(), aerodromeService.recupererAerodrome(4l).get(), 240));
		}
		if (volService.recupererVols().isEmpty()) {
			Duree duree = dureeService.recupererDuree(1l).get();
			
			calendar.set(Calendar.YEAR, 2020);
			calendar.set(Calendar.MONTH, Calendar.MARCH);
			calendar.set(Calendar.DAY_OF_MONTH, 24);
			
			for (int i = 0; i < 7; i++) {
				calendar.add(Calendar.DAY_OF_MONTH, 1);
				for (int j = 0; j < 8; j++) {
					calendar.set(Calendar.HOUR_OF_DAY, random.nextInt(24-(duree.getDureeDuVolEnMinutes()/60)));
					Date dateHeureDepart = calendar.getTime();
					calendar.add(Calendar.MINUTE, duree.getDureeDuVolEnMinutes());
					Date dateHeureArrivee = calendar.getTime();
					Avion avion = avionService.recupererAvion((long) 1+random.nextInt(avionService.recupererAvions().size()-1)).get();
					volService.creerVol(new Vol(duree.getAerodromeDepart(), duree.getAerodromeArrivee(), dateHeureDepart, dateHeureArrivee, avion, (float) 30+random.nextInt(500)));
				}
			}
		}
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder, WebRequest request) {
		//binder pour date
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(simpleDateFormat, true));
		//binder pour classe
		binder.registerCustomEditor(TypeAppareil.class, "typeAppareil", new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				setValue((text.equals("")) ? null : typeAppareilService.recupererTypeAppareil(Long.parseLong(text)));
			}
		});
		binder.registerCustomEditor(Avion.class, "avion", new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				setValue((text.equals("")) ? null : avionService.recupererAvion(Long.parseLong(text)));
			}
		});
		binder.registerCustomEditor(Aerodrome.class, "aerodromeDepart", new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				setValue((text.equals("")) ? null : aerodromeService.recupererAerodrome(Long.parseLong(text)));
			}
		});
		binder.registerCustomEditor(Aerodrome.class, "aerodromeArrivee", new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				setValue((text.equals("")) ? null : aerodromeService.recupererAerodrome(Long.parseLong(text)));
			}
		});
		binder.registerCustomEditor(Passager.class, "passager", new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				setValue((text.equals("")) ? null : passagerService.recupererPassager(Long.parseLong(text)));
			}
		});
		binder.registerCustomEditor(Vol.class, "vol", new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				setValue((text.equals("")) ? null : volService.recupererVol(Long.parseLong(text)));
			}
		});
		binder.registerCustomEditor(List.class, "services", new CustomCollectionEditor(List.class) {
			 @Override
			 public Object convertElement(Object objet) {
				 Long id = Long.parseLong((String) objet);
				 return serviceService.recupererService(id);
			 }
		 });
	}
}
