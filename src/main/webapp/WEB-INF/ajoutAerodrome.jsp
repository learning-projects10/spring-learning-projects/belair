<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Ajouter un aerodrome</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/style.css">
	</head>
	<body class="m-4">
	
		<header>
			<h1>Nouveau aerodrome</h1>
			<jsp:include page="lien.jsp" />
		</header>
		
		<form:form modelAttribute="aerodrome" action="ajoutAerodrome" method="post">
		
			<div class="form-group">
				<form:label path="nom">Nom :</form:label>
				<form:input class="form-control" path="nom" />
				<form:errors path="nom" cssClass="erreur" />
			</div>
			
			<form:button class="btn btn-success">Ajouter l'aerodrome</form:button>
		</form:form>
	</body>
</html>
