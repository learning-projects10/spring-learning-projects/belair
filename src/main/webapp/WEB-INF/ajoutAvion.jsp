<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Ajouter un avion</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/style.css">
	</head>
	<body class="m-4">
	
		<header>
			<h1>Nouvelle avion</h1>
			<jsp:include page="lien.jsp" />
		</header>
		
		<form:form modelAttribute="avion" action="ajoutAvion" method="post">
		
			<div class="form-group">
				<form:label path="immatriculation">Immatriculation : </form:label>
				<form:input class="form-control" path="immatriculation" />
				<form:errors path="immatriculation" cssClass="erreur" />
			</div>
			
			<div class="form-group">
				<form:label path="datePremierVol">Date de premier vol : </form:label>
				<form:input class="form-control" path="datePremierVol" type="datetime-local"/>
				<form:errors path="datePremierVol" cssClass="erreur" />
			</div>
			
			<div class="form-group">
				<form:label path="typeAppareil">Type appareil : </form:label>
				<form:select class="form-control" path="typeAppareil">
					<form:option value="0">Type appareil</form:option>
					<form:options items="${typeAppareils}" itemValue="id" itemLabel="nom"/>
				</form:select>
				<form:errors path="typeAppareil" cssClass="erreur" />
			</div>
			
			<form:button class="btn btn-success">Ajouter l'avion</form:button>
		</form:form>
	</body>
</html>
